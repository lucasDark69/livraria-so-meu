﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace BibliotecaAluguel.Telas
{
    public partial class frmMenuLogin : Form
    {
        public frmMenuLogin()
        {
            InitializeComponent();
        }

        private void BunifuFlatButton1_Click(object sender, EventArgs e)
        {
            try
            {
                // pega os parâmetros
                string user = txtlogin.Text;
                string senha = txtsenha.Text;




            
                                         
                // chama o database
                DataBase.model.libraryEntities db = new DataBase.model.libraryEntities();
                DataBase.model.tb_login validação = db.tb_login.FirstOrDefault(l => l.nm_usuario == user && l.nm_senha == senha);

                // faz uma validação se retornar null o usuario não e logado

                if (validação != null)
                {
                    Telas.frmMenu tela = new frmMenu();
                    tela.Show();
                    // this.Hide();
                }
                else
                {
                    MessageBox.Show("usuario ou senha invalidos");
                    return;

                }
            }
            catch (Exception)
            {

                MessageBox.Show("erro tente mais tarde", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
        }
        private void BunifuFlatButton2_Click(object sender, EventArgs e)
        {
            Telas.frmCadastrar tela = new frmCadastrar();
            tela.Show();
        }

        private void BunifuFlatButton3_Click(object sender, EventArgs e)
        {
            Telas.frmAdminADM tela = new frmAdminADM();
            tela.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}


            
            

            
