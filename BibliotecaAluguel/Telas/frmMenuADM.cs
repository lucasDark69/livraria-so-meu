﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BibliotecaAluguel.Telas
{
    public partial class frmMenuADM : Form
    {
        public frmMenuADM()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            frmInsert tela = new frmInsert();
            tela.Show();
            

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            //abre tela consultar
            Telas.frmConsultar tela = new frmConsultar();
            tela.Show();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            // fecha e aplicação
            Application.Exit();
        }

        private void BunifuFlatButton1_Click(object sender, EventArgs e)
        {
            //abre tela menu de login
            Telas.frmMenuLogin telas = new frmMenuLogin();
            telas.Show();
            this.Hide();
        }

        private void Button4_Click(object sender, EventArgs e)
        {

        }

        private void BunifuFlatButton2_Click(object sender, EventArgs e)
        {
            frmInsert tela = new frmInsert();
            tela.Show();
            this.Hide();
        }

        private void BunifuFlatButton3_Click(object sender, EventArgs e)
        {
            Telas.frmConsultar tela = new frmConsultar();
            tela.Show();
            this.Hide();
        }

        private void BunifuFlatButton5_Click(object sender, EventArgs e)
        {
            Telas.frmDelete tela = new frmDelete();
            tela.Show();
            this.Hide();
        }

        private void Button3_Click(object sender, EventArgs e)
        {

        }
    }
}
