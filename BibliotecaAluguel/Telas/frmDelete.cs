﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BibliotecaAluguel.Telas
{
    public partial class frmDelete : Form
    {
        public frmDelete()
        {
            InitializeComponent();
            this.CarregarCombo();
        }

        private void CarregarCombo()
        {
            // faz a consulta no banco de dados 
            DataBase.databaselibrary db = new DataBase.databaselibrary();
            List<DataBase.model.tb_livros> livros = db.Listar();

            // carrega as informações no combo 
            cbolivros.DisplayMember = nameof(DataBase.model.tb_livros.nm_livro);
            cbolivros.DataSource = livros;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
           
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {

                DataBase.model.tb_livros livro = cbolivros.SelectedItem as DataBase.model.tb_livros;
                int idlivro = livro.id_livro;

                Business.business busi = new Business.business();
                busi.remove(idlivro);

                MessageBox.Show("deletado", "sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception)
            {

                MessageBox.Show("erro tente mais tarde", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
