﻿namespace BibliotecaAluguel.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Button2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SlateGray;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(-3, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(721, 37);
            this.panel2.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(323, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "bem vindo";
            // 
            // Button2
            // 
            this.Button2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button2.BackColor = System.Drawing.SystemColors.Highlight;
            this.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button2.BorderRadius = 0;
            this.Button2.ButtonText = "Emprestar um livro";
            this.Button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button2.DisabledColor = System.Drawing.Color.Gray;
            this.Button2.Iconcolor = System.Drawing.Color.Transparent;
            this.Button2.Iconimage = ((System.Drawing.Image)(resources.GetObject("Button2.Iconimage")));
            this.Button2.Iconimage_right = null;
            this.Button2.Iconimage_right_Selected = null;
            this.Button2.Iconimage_Selected = null;
            this.Button2.IconMarginLeft = 0;
            this.Button2.IconMarginRight = 0;
            this.Button2.IconRightVisible = true;
            this.Button2.IconRightZoom = 0D;
            this.Button2.IconVisible = true;
            this.Button2.IconZoom = 90D;
            this.Button2.IsTab = false;
            this.Button2.Location = new System.Drawing.Point(160, 220);
            this.Button2.Name = "Button2";
            this.Button2.Normalcolor = System.Drawing.SystemColors.Highlight;
            this.Button2.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Button2.OnHoverTextColor = System.Drawing.Color.White;
            this.Button2.selected = false;
            this.Button2.Size = new System.Drawing.Size(407, 65);
            this.Button2.TabIndex = 7;
            this.Button2.Text = "Emprestar um livro";
            this.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button2.Textcolor = System.Drawing.Color.White;
            this.Button2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button2.Click += new System.EventHandler(this.Button2_Click_1);
            // 
            // Button1
            // 
            this.Button1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button1.BackColor = System.Drawing.SystemColors.Highlight;
            this.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button1.BorderRadius = 0;
            this.Button1.ButtonText = "Consultar livros";
            this.Button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button1.DisabledColor = System.Drawing.Color.Gray;
            this.Button1.Iconcolor = System.Drawing.Color.Transparent;
            this.Button1.Iconimage = ((System.Drawing.Image)(resources.GetObject("Button1.Iconimage")));
            this.Button1.Iconimage_right = null;
            this.Button1.Iconimage_right_Selected = null;
            this.Button1.Iconimage_Selected = null;
            this.Button1.IconMarginLeft = 0;
            this.Button1.IconMarginRight = 0;
            this.Button1.IconRightVisible = true;
            this.Button1.IconRightZoom = 0D;
            this.Button1.IconVisible = true;
            this.Button1.IconZoom = 90D;
            this.Button1.IsTab = false;
            this.Button1.Location = new System.Drawing.Point(160, 149);
            this.Button1.Name = "Button1";
            this.Button1.Normalcolor = System.Drawing.SystemColors.Highlight;
            this.Button1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Button1.OnHoverTextColor = System.Drawing.Color.White;
            this.Button1.selected = false;
            this.Button1.Size = new System.Drawing.Size(407, 65);
            this.Button1.TabIndex = 6;
            this.Button1.Text = "Consultar livros";
            this.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button1.Textcolor = System.Drawing.Color.White;
            this.Button1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button1.Click += new System.EventHandler(this.Button1_Click_2);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::BibliotecaAluguel.Properties.Resources.error;
            this.pictureBox1.Location = new System.Drawing.Point(684, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(22, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "Consultar emprestimo";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton1.Iconimage")));
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 90D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(160, 291);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.SystemColors.MenuHighlight;
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(407, 65);
            this.bunifuFlatButton1.TabIndex = 8;
            this.bunifuFlatButton1.Text = "Consultar emprestimo";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Click += new System.EventHandler(this.bunifuFlatButton1_Click_1);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 450);
            this.Controls.Add(this.bunifuFlatButton1);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMenu";
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuFlatButton Button1;
        private Bunifu.Framework.UI.BunifuFlatButton Button2;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
    }
}