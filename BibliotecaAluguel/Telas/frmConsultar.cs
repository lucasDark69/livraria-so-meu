﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BibliotecaAluguel.Telas
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {

            try
            {
                // consulta no banco de dados fazendo um inner join nas tabelas Categorias de livros
                DataBase.model.libraryEntities db = new DataBase.model.libraryEntities();
                var CategoriaInnerJoinLivros = db.tb_livros
                    .Join(db.tb_categorias, l => l.id_categoria, c => c.id_categoria, (l, c) => new { l, c })
                    .Select(s => new
                    {
                        s.l.nm_livro,
                        s.c.nm_categoria,
                        s.l.nm_autor,
                        s.l.nr_avaliaçao,
                        s.l.dt_publicaçao
                    }); ;
                        
                // coloca as informações coletadas em uma lista 
                var innerjoin = CategoriaInnerJoinLivros.ToList();
                // exibe na grid
                grid.DataSource = innerjoin;
            }
            catch (Exception)
            {

                MessageBox.Show("erro tente mais tarde","erro",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }


        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                string autor = txtautor.Text.Trim();

                if (autor == string.Empty)
                {
                    MessageBox.Show("Informe o autor", "erro");
                    return;
                }

                // consulta no banco de dados fazendo um inner join nas tabelas Categorias de livros
                DataBase.model.libraryEntities db = new DataBase.model.libraryEntities();
                var CategoriaInnerJoinLivros = db.tb_livros
                    .Join(db.tb_categorias, l => l.id_categoria, c => c.id_categoria, (l, c) => new { l, c })
                    .Select(s => new
                    {
                        s.l.nm_livro,
                        s.c.nm_categoria,
                        s.l.nm_autor,
                        s.l.nr_avaliaçao,
                        s.l.dt_publicaçao
                    }).Where(l => l.nm_autor == autor);

                // coloca as informações coletadas em uma lista 
                var innerjoin = CategoriaInnerJoinLivros.ToList();
                // exibe na grid
                grid.DataSource = innerjoin;
            }
            catch (Exception)
            {

                MessageBox.Show("erro tente mais tarde", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

