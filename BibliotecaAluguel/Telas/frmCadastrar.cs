﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BibliotecaAluguel.Telas
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }

        private void BunifuFlatButton1_Click(object sender, EventArgs e)
        {
            try
            {
                // chama o database
                DataBase.model.tb_login l = new DataBase.model.tb_login();

                if (txtuser.Text == string.Empty || txtsenha.Text == string.Empty || txtemail.Text == string.Empty)
                {
                    MessageBox.Show("complete os campos");
                    return;
                }

                // carrega o modelo
                l.nm_usuario = txtuser.Text.Trim();
                l.nm_senha = txtsenha.Text.Trim();
                l.nm_email = txtemail.Text.Trim();


                // passa para o banco
                DataBase.model.libraryEntities db = new DataBase.model.libraryEntities();
                db.tb_login.Add(l);

                // salva no banco
                db.SaveChanges();

                MessageBox.Show("obrigado","sucesso",MessageBoxButtons.OK,MessageBoxIcon.Information);

                Telas.frmMenuLogin tela = new frmMenuLogin();
                tela.Show();
                this.Hide();

            }
            catch (Exception)
            {

                MessageBox.Show("Usuario já existe","erro", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            Telas.frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
    }
}

            

