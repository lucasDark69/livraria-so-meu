﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaAluguel.Business
{
    class business
    {
        public void alterar(DataBase.model.tb_emprestimo em)
        {
            string IDemprestimo = Convert.ToString(em.id_emprestimo);

            if (em.dt_devolucao == null || IDemprestimo == string.Empty)
            {
                throw new ArgumentException("complete os campos");

            }

            DataBase.databaselibrary db = new DataBase.databaselibrary();
            db.alterar(em);



        }
        public void emprestimo(DataBase.model.tb_emprestimo em)
        {

            if (em.dt_emprestimo == null)
            {
                throw new ArgumentException("complete os campos");

            }


            DataBase.databaselibrary db = new DataBase.databaselibrary();
            db.emprestimo(em);


        }
        public void remove (int idlivro)
        {
          
            string id = Convert.ToString(idlivro);

            if (id == string.Empty )
            {
                throw new ArgumentException("informe o ID");

            }

            DataBase.databaselibrary db = new DataBase.databaselibrary();
            db.remove(idlivro);

        }
    }
}
